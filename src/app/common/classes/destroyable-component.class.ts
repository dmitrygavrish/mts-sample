import { OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export abstract class DestroyableComponentClass implements OnDestroy {
    protected _destroyed: boolean = false;
    protected readonly _destroy$$: Subject<void> = new Subject();

    public ngOnDestroy(): void {
        this._destroyed = true;

        this._destroy$$.next();
        this._destroy$$.complete();
    }

    protected _observeSafe<T = any>(obs: Observable<T>): Observable<T> {
        return obs.pipe(takeUntil(this._destroy$$));
    }
}
