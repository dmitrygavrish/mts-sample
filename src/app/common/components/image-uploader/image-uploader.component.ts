import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { fromEvent, Observable, Subject } from 'rxjs';
import { filter, mergeMap, take } from 'rxjs/operators';
import { DestroyableComponentClass } from '../../classes/destroyable-component.class';
import { DragSwitchable } from '../../interfaces/drag-switchable.interface';

/**
 * Image uploader;
 * Notifies parent component, when user uploads image file;
 */
@Component({
    selector: 'app-image-uploader',
    templateUrl: './image-uploader.component.html',
    styleUrls: ['./image-uploader.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class ImageUploaderComponent extends DestroyableComponentClass implements DragSwitchable, OnInit {
    private readonly _file$$: Subject<File> = new Subject();

    public isDragActive: boolean = false;

    /**
     * Emits source string of uploaded image;
     */
    @Output()
    public readonly imageUpload: EventEmitter<string> = new EventEmitter();

    public ngOnInit(): void {
        this._handleFiles();
    }

    public onFileInputChange(event: Event): void {
        this._pipeFileList((event.target as HTMLInputElement).files);
    }

    public onFileDrop(event: DragEvent): void {
        this._pipeFileList(event.dataTransfer.files);
    }

    /**
     * Initiates working with user-added files;
     */
    private _handleFiles(): void {
        this._observeSafe(this._file$$
            .pipe(
                filter((file: File) => {
                    const isImageFile: boolean = this._isImageFile(file.type);

                    if (!isImageFile) {
                        this._notifyIncorrectFile(file.name);
                    }

                    return isImageFile;
                }),
                mergeMap((image: File) => {
                    const fileReader: FileReader = new FileReader();
                    const readerLoad$: Observable<Event> = fromEvent(fileReader, 'load').pipe(take(1));

                    fileReader.readAsDataURL(image);

                    return readerLoad$;
                })
            ))
            .subscribe((event: ProgressEvent) => {
                this.imageUpload.emit((event.currentTarget as FileReader).result.toString());
            });
    }

    /**
     * Pipes file list into file subject one-by-one;
     */
    private _pipeFileList(files: FileList): void {
        Array.from(files).forEach((file: File) => {
            this._file$$.next(file);
        });
    }

    private _notifyIncorrectFile(fileName: string): void {
        alert(`Файл ${fileName} не является изображением`);
    }

    private _isImageFile(fileType: string): boolean {
        return fileType.indexOf('image/') === 0;
    }
}
