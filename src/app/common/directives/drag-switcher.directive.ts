import { Directive, HostListener, Input } from '@angular/core';
import { DragSwitchable } from '../interfaces/drag-switchable.interface';

/**
 * Allows customization of elements, when those elements are being dragged over;
 */
@Directive({
    selector: '[appDragSwitcher]'
})
export class DragSwitcherDirective {
    @Input()
    public appDragSwitcher: DragSwitchable;

    @HostListener('dragover')
    @HostListener('dragenter')
    public onDetectDrag(): void {
        this.appDragSwitcher.isDragActive = true;
    }

    @HostListener('dragleave')
    @HostListener('dragend')
    @HostListener('drop')
    public onDetectDragCancel(): void {
        this.appDragSwitcher.isDragActive = false;
    }
}
