import { Directive, HostListener } from '@angular/core';

/**
 * Nullifies default behaviour of browser-native drag events, i.e. prevents
 * replacing current browser page with file contents;
 */
@Directive({
    selector: '[appDragNullifier]'
})
export class DragNullifierDirective {
    @HostListener('drag', ['$event'])
    @HostListener('dragstart', ['$event'])
    @HostListener('dragend', ['$event'])
    @HostListener('dragenter', ['$event'])
    @HostListener('dragover', ['$event'])
    @HostListener('dragleave', ['$event'])
    @HostListener('drop', ['$event'])
    public onDragEvent(event: Event): void {
        event.preventDefault();
        event.stopPropagation();
    }
}
