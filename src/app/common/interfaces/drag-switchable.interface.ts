export interface DragSwitchable {
    isDragActive: boolean;
}
