import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

/**
 * Storage service for images
 */
@Injectable({providedIn: 'root'})
export class ImageService {
    private readonly _images$$: BehaviorSubject<string[]> = new BehaviorSubject([]);
    public readonly images$: Observable<string[]> = this._images$$.asObservable();

    public addImages(images: string[]): void {
        this._images$$.next([...this._images$$.getValue(), ...images]);
    }
}
