import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { DragNullifierDirective } from './common/directives/drag-nullifier.directive';
import { DragSwitcherDirective } from './common/directives/drag-switcher.directive';
import { ImageService } from './common/services/image.service';
import { ImageUploaderComponent } from './common/components/image-uploader/image-uploader.component';

@NgModule({
    declarations: [
        AppComponent,
        ImageUploaderComponent,
        DragNullifierDirective,
        DragSwitcherDirective,
    ],
    imports: [
        BrowserModule
    ],
    providers: [
        ImageService
    ],
    bootstrap: [
        AppComponent
    ]
})
export class AppModule {
}
