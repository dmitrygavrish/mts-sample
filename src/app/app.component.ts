import { Component, HostListener, OnInit } from '@angular/core';
import { ImageService } from './common/services/image.service';
import { DestroyableComponentClass } from './common/classes/destroyable-component.class';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent extends DestroyableComponentClass implements OnInit {
    public images: string[] = [];

    public currentImageIndex: number | null = null;

    constructor(
        private readonly _imageService: ImageService
    ) {
        super();
    }

    public ngOnInit(): void {
        this._handleImages();
    }

    public onImagePress(image: string, index: number): void {
        this.currentImageIndex = index;
    }

    public onImageUpload(image: string): void {
        this._imageService.addImages([image]);
    }

    public onSwitchLeftPress(): void {
        this._setPreviousImage();
    }

    public onSwitchRightPress(): void {
        this._setNextImage();
    }

    public trackImageByIndex(index: number, image: string): number {
        return index;
    }

    /**
     * Handles arrow keys pressing;
     * If left arrow or right arrow key is pressed,
     * then active image in gallery is switched in according direction;
     */
    @HostListener('window:keydown', ['$event'])
    public onWindowKeyDown(event: KeyboardEvent): void {
        event.stopPropagation();

        const keyLow: string | null = event.key ? event.key.toLowerCase() : null;

        if (keyLow === 'arrowleft') {
            this._setPreviousImage();
        } else if (keyLow === 'arrowright') {
            this._setNextImage();
        }
    }

    /**
     * Stores images in component's context, when images change;
     */
    private _handleImages(): void {
        this._observeSafe(this._imageService.images$)
            .subscribe((images: string[]) => {
                this.images = images;
            });
    }

    private _setNextImage(): void {
        const maxIndex: number = this.images.length - 1;
        const expectingIndex: number = this.currentImageIndex + 1;

        this.currentImageIndex = expectingIndex > maxIndex
            ? 0
            : expectingIndex;
    }

    private _setPreviousImage(): void {
        const minIndex: number = 0;
        const expectingIndex: number = this.currentImageIndex - 1;

        this.currentImageIndex = expectingIndex < minIndex
            ? this.images.length - 1
            : expectingIndex;
    }
}
