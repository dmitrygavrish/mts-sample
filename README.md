# MtsSample

* This application is an attempt to solve a task from MTS company.
* Application was created with angular/cli v. 8.3.5 & has all build & serve options of angular/cli
* Supports latest versions of browsers + IE11, IE11 is supported only with `ng build --prod` command (polyfills are added), serving tools for IE11 are not provided within application 
